jQuery.support.cors = true;
let timeoutVar;
function update() {
    let tableEntry =``;

    $.ajax({
        url: 'https://cors-anywhere.herokuapp.com/https://www.bloomberght.com/piyasa/intradaydata/dolar',

        success: function(data) {
            let a = JSON.parse(data);
            tableEntry += `<table><tr><th>Time</th><th>Value</th><th>Updated At</th></tr></table>`;
            let updatedAt =  new Date(Date.now()).toLocaleString();

            for(let i = 11;i>0;i--){
                let time = new Date(a['SeriesData'][a['SeriesData'].length-i][0]).toLocaleString();
                let value = a['SeriesData'][a['SeriesData'].length-i][1];
                tableEntry += `<tr><td>${time}</td><td>${value} TRY</td><td>${updatedAt}</td></tr>`;
            }
            $("#my-table table").html(tableEntry);

        },
        error: function(jqXHR, textStatus, errorThrown) {
            alert(jqXHR.status);
        },
    }).then(()=>{timeoutVar= setTimeout(update, 60000)})
}
$(document).ready(update());
$("#new-button").click(function () {
    clearInterval(timeoutVar);
    update()
})